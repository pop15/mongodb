// ใช้คำสั่งนี้

protoc --proto_path=proto proto/pingpong.proto --go_out=plugins=grpc:pb

protoc --go_out=plugins=grpc:. proto/lersi_kobutra.proto

protoc --go_out=. --go_opt=paths=api \ --go-grpc_out=. --go-grpc_opt=paths=api \proto/route_guide.proto
protoc --go-grpc_out=./gen --go-grpc_opt=paths=proto/lersi_kobutra.proto

protoc -I. --go-grpc_out=proto lersi_kobutra.proto

protoc --go_out=paths=proto:./ -I. lersi_kobutra.proto
protoc --go_out=plugins=grpc:proto pingpong.proto
protoc --go_out=plugins=grpc:./gen --go_opt=paths=source_relative lersi_kobutra.proto

protoc --go_out=. --go_opt=paths=source_relative lersi_kobutra.proto 

protoc --go_out=plugins=grpc:. proto/lersi_kobutra.proto
protoc --go_out=plugins=grpc:. proto/lersi_kobutra_set.proto


MongoDB install 
1.ติดตั้งเสร็จแล้ว ให้ไป tap package ของ mongoDB มาด้วยคำสั่ง
brew tap mongodb/brew
2.Install mongoDB ด้วยคำสั่ง
brew install mongodb-community
3.การใช้งาน mongoDB จำเป็นต้องสร้าง /data/db โฟลเดอร์ เพื่อใช้งาน MongoDB โดยใช้คำสั่ง
sudo mkdir -p /System/Volumes/Data/data/db
4.ช้คำสั่งนี้เพื่อให้สิทธิ์การเข้าถึงโฟลเดอร์
sudo chown -R `id -un` /System/Volumes/Data/data/db
5.start-stop MongoDB ผ่าน brew services.
brew services run mongodb-community