module example.com/m

go 1.16

require (
	github.com/OWASP/Amass/v3 v3.13.4 // indirect
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.3.0 // indirect
	github.com/mcuadros/go-version v0.0.0-20190830083331-035f6764e8d2 // indirect
	github.com/netflix/conductor/client/go v0.0.0-20210401201512-216960d11116 // indirect
	github.com/parnurzeal/gorequest v0.2.16 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/projectdiscovery/naabu/v2 v2.0.4 // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/spf13/viper v1.8.1
	gitlab.com/pop15/mongodb v0.0.0-20210624031118-3fd6a402fe94
	go.mongodb.org/mongo-driver v1.5.3
	golang.org/x/net v0.0.0-20210614182718-04defd469f4e // indirect
	google.golang.org/grpc v1.39.0
	google.golang.org/protobuf v1.27.1
	gopkg.in/ini.v1 v1.62.0 // indirect
	moul.io/http2curl v1.0.0 // indirect
)
