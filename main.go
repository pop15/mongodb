package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"os"
	"reflect"

	"github.com/spf13/viper"
	pb "gitlab.com/pop15/mongodb/api/lersri_kubotra_api"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

//Issue - struct to map with mongodb documents
type Issue struct {
	Jobid  string `bson:"jobid"`
	Output string `bson:"output"`
}

var collection *mongo.Collection
var ctx = context.TODO()

var (
	tls      = flag.Bool("srv_tls", false, "Connection uses TLS if true, else plain TCP")
	certFile = flag.String("srv_cert_file", "", "The TLS certificate file")
	keyFile  = flag.String("srv_key_file", "", "The TLS key file")
	addr     = flag.String("srv_addr", "1.2.3.4", "The server address")
	port     = flag.Int("srv_port", 65536, "The server port")
)

func initFlag() {
	// server
	*tls = viper.GetBool("API.TLS")
	*certFile = viper.GetString("API.CertFile")
	*keyFile = viper.GetString("API.KeyFile")
	*addr = viper.GetString("API.Addr")
	*port = viper.GetInt("API.Port")
	// database

}

type DataRequest struct {
	JobID  string `json:"JobID"`
	Output string `json:"Output"`
}

type DataRequestByJobId struct {
	JobID      string `json:"JobID"`
	Attributes string `json:"Attributes"`
}

// ประกาศ Struct เพื่อใช้เก็บค่า client
type lersikobutraServerImpl struct {
}

// ส่งค่าออกไป
func (s *lersikobutraServerImpl) GetResult(ctx context.Context, save_request *pb.JobRequest) (*pb.JobResponse, error) {
	fmt.Println("Requesting...")

	// insert data
	dbname := viper.GetString("Database.DBName")
	dbpass := viper.GetString("Database.Password")
	dbuser := viper.GetString("Database.User")
	mongodb_conn := viper.GetString("Database.Mongodb_conn")

	clientOptions := options.Client().ApplyURI(mongodb_conn).
		SetAuth(options.Credential{
			AuthSource: dbname, Username: dbuser, Password: dbpass,
		})

	//fmt.Printf("dbname=\ntls = %s\ndbpass= %s\ndbuser = %s", dbname, dbpass, dbpass, dbuser)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	collection = client.Database(dbname).Collection("tossakan")

	// data_scan := DataRequest{
	// 	JobID: save_request.JobID,
	// }

	var result DataRequest
	filter := bson.D{{"jobid", save_request.JobID}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Found a single document: %+v\n", result)

	resp := pb.JobResponse{
		JobID:  save_request.JobID,
		Output: result.Output,
	}

	err = client.Disconnect(context.TODO())

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection to MongoDB closed.")

	//fmt.Print("Domain:" + save_request.Domain + " IP:" + save_request.Ip + " ScannedTime:" + save_request.ScannedTime + " Tool:" + save_request.Tool)

	return &resp, nil
}

// เอาไว้รับค่าเมื่อมีการส่งเข้ามาผ่านทาง grpc
func (s *lersikobutraServerImpl) SaveResult(ctx context.Context, save_request *pb.SaveRequest) (*pb.SaveResponse, error) {
	fmt.Println("SaveRequesting")
	resp := pb.SaveResponse{
		JobID: "1",
		Msg:   "Success",
	}

	// insert data
	dbname := viper.GetString("Database.DBName")
	dbpass := viper.GetString("Database.Password")
	dbuser := viper.GetString("Database.User")
	mongodb_conn := viper.GetString("Database.Mongodb_conn")

	clientOptions := options.Client().ApplyURI(mongodb_conn).
		SetAuth(options.Credential{
			AuthSource: dbname, Username: dbuser, Password: dbpass,
		})

	//fmt.Printf("dbname=\ntls = %s\ndbpass= %s\ndbuser = %s", dbname, dbpass, dbpass, dbuser)
	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		log.Fatal(err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	collection = client.Database(dbname).Collection("tossakan")

	data_scan := DataRequest{
		JobID:  save_request.JobID,
		Output: save_request.Output,
	}

	//fmt.Println("TYPE:", reflect.TypeOf(data_scan), "\n")
	var result DataRequest
	filter := bson.D{{"jobid", save_request.JobID}}
	err = collection.FindOne(context.TODO(), filter).Decode(&result)

	fmt.Println("Insert newID:", result)
	if err != nil {

		//log.Fatal(err)
		// InsertOne() method Returns mongo.InsertOneResult
		result, insertErr := collection.InsertOne(ctx, data_scan)
		if insertErr != nil {
			fmt.Println("Insert ERROR:", insertErr)
			os.Exit(1) // safely exit script on error
		} else {
			fmt.Println("Insert result type: ", reflect.TypeOf(result))
			fmt.Println("Insert API result:", result)

			// get the inserted ID string
			newID := result.InsertedID
			fmt.Println("Insert newID:", newID)
			//fmt.Println("Insert newID type:", reflect.TypeOf(newID))
		}

	} else {

		filter := bson.D{{"jobid", save_request.JobID}}
		update := bson.M{"$set": bson.M{"output": save_request.Output}}

		updateResult, err := collection.UpdateOne(context.TODO(), filter, update)
		if err != nil {
			log.Fatal(err)
		}

		fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)

	}

	err = client.Disconnect(context.TODO())

	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection to MongoDB closed.")

	//fmt.Print("Domain:" + save_request.Domain + " IP:" + save_request.Ip + " ScannedTime:" + save_request.ScannedTime + " Tool:" + save_request.Tool)

	return &resp, nil
}

func main() {
	//อ่านค่า config.yaml
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}

	initFlag()
	flag.Parse()

	server := lersikobutraServerImpl{}

	//แสดงผลการตั้งค่า server
	fmt.Printf("Server Config:\ntls = %v\ncertFile = %s\nkeyFile = %s\naddr = %s\nport = %d\n", *tls, *certFile, *keyFile, *addr, *port)

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", *addr, *port))
	checkError(viper.GetString("Error_Message.listen_error"), err)
	var opts []grpc.ServerOption
	if *tls {
		creds, err := credentials.NewServerTLSFromFile(*certFile, *keyFile)
		checkError(viper.GetString("Error_Message.TLS_creds_error"), err)
		opts = []grpc.ServerOption{grpc.Creds(creds)}
	}
	grpcServer := grpc.NewServer(opts...)
	// register save data
	pb.RegisterLersiKobutra_Server_SaveServer(grpcServer, &server)

	// register get data
	pb.RegisterLersiKobutra_Server_GetServer(grpcServer, &server)

	// register upload image data
	pb.RegisterLersiKobutra_Server_UploadImagesServer(grpcServer, &server)

	//display running addr and port
	fmt.Printf("Server is running at %s:%d\n", *addr, *port)

	grpcServer.Serve(lis)

}

func checkError(message string, err error) {
	if err != nil {
		log.Fatalf("%s: %v", message, err)
	}
}
