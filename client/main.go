package main

import (
	"context"
	"fmt"
	"time"

	pb "gitlab.com/pop15/mongodb/api/lersri_kubotra_api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DataRequest struct {
	JobID  string
	Output string
}

func SaveData(client pb.LersiKobutra_Server_SaveClient, jobID string, output string) (*pb.SaveResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	ping := pb.SaveRequest{
		JobID:  jobID,
		Output: output,
	}

	lsi, err := client.SaveResult(ctx, &ping)
	statusCode := status.Code(err)
	if statusCode != codes.OK {
		return nil, err
	}

	fmt.Printf("Save: %d, statusCode: %s\n", lsi.JobID, statusCode.String())

	return lsi, err
}

func SaveDBClient(jobID string, output string) {
	opts := []grpc.DialOption{grpc.WithInsecure()}
	conn, err := grpc.Dial("127.0.0.1:10000", opts...)
	if err != nil {
		panic(err)
	}

	client := pb.NewLersiKobutra_Server_SaveClient(conn)
	_, err = SaveData(client, jobID, output)
	if err != nil {
		panic(err)
	}
	fmt.Println("Finish Pinging")
}

/// fortest
func main() {

	SaveDBClient("2", ` {
		"domain": "wcm.incognitolab.com",
		"ips": [
			{
				"ip": "4.74.253.204",
				"ports": 22222
			}
		]
	}`)

}
