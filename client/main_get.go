package main

import (
	"context"
	"fmt"
	"time"

	pb "gitlab.com/pop15/mongodb/api/lersri_kubotra_api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DataRequest struct {
	JobID  string
	Output string
}

func GetData(client pb.LersiKobutra_Server_GetClient, jobID string) (*pb.JobResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	ping := pb.JobRequest{
		JobID: jobID,
	}

	lsi, err := client.GetResult(ctx, &ping)
	statusCode := status.Code(err)
	if statusCode != codes.OK {
		return nil, err
	}

	fmt.Printf("Get : %d, statusCode: %s\n", lsi.Output, statusCode.String())

	return lsi, err
}

func GetDBClient(jobID string) {
	opts := []grpc.DialOption{grpc.WithInsecure()}
	conn, err := grpc.Dial("127.0.0.1:10000", opts...)
	if err != nil {
		panic(err)
	}

	client := pb.NewLersiKobutra_Server_GetClient(conn)
	_, err = GetData(client, jobID)
	if err != nil {
		panic(err)
	}
	fmt.Println("Finish Pinging")
}

/// fortest
func main() {

	GetDBClient("1")

}
