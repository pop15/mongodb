gen:
	protoc -I=proto --go_out=. proto/*.proto
serverstart:
	go run server/main.go
serverstop: 
	go stop server/main.go
	
clientstart:
	go run client/main.go
clientstop:
	go run client/main.go
