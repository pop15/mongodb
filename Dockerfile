# Dockerfile References: https://docs.docker.com/engine/reference/builder/


# Start from the latest golang base image
FROM golang:1.16.0-alpine3.13 AS builder
RUN apk add build-base libpcap-dev
# RUN GO111MODULE=on go get -v github.com/projectdiscovery/naabu/v2/cmd/naabu


USER root
# Add Maintainer Info
LABEL maintainer="popinlive@gmail.com"

#ENV GO111MODULE=on
#RUN GO111MODULE=on go get -v github.com/projectdiscovery/naabu/v2/cmd/naabu
#naabu -version
RUN apk add nmap libpcap-dev
# Set the Current Working Directory inside the container
WORKDIR /go/src/app/

# /go/src/app/naabu/v2/cmd/naabu

# Copy go mod and sum files
COPY go.mod ./

# Download all dependencies. Dependencies will be cached if the go.mod and go.sum files are not changed
RUN go mod download



# Copy the source from the current directory to the Working Directory inside the container
COPY . .

RUN go get -d -v ./...


#RUN go install -v ./...

# Build the Go app
RUN go build -o main .
CMD ["./main"]
#ENTRYPOINT ["./main", "main.go"]
# ENV PATH "$PATH:/go/src/app/"

# CMD ["./naabu"]
# ENTRYPOINT ["./main","main.go"]
